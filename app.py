from flask import Flask
from flask_cors import CORS
from Controllers.controller import main_route

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

app.register_blueprint(main_route, url_prefix="/api")


if __name__ == '__main__':
    app.run()
