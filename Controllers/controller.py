from flask import Blueprint
from flask_cors import cross_origin

main_route = Blueprint("api", __name__)


@main_route.route("/", methods=['GET'])
@cross_origin()
def leaves():
    return "This tree has leaves"


@main_route.route("/roots", methods=['POST'])
@cross_origin()
def roots():
    return "And roots as well"


@main_route.route("/rings")
@main_route.route("/rings/<int:year>")
@cross_origin()
def rings(year=None):
    return "Looking at the rings for {year}".format(year=year)
